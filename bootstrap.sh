#!/bin/bash

echo "****** Start nginx ******"
sudo systemctl start nginx
echo "****** Enable nginx ******"
sudo systemctl enable nginx
echo "****** Install nano ******"
sudo yum install -y nano